/**
 * @author Alejandro Suarez Hernandez
 * ExpressionElement abstract class
 */


public abstract class ExpressionElement
    implements Comparable<ExpressionElement>
{
  public abstract int numericalValue();

  @Override
  public int hashCode()
  {
    return numericalValue();
  }

  /**
   * The numerical value of the ExpressionElement instances is used to compare
   * them
   */
  @Override
  public int compareTo(ExpressionElement el)
  {
    int x = numericalValue();
    int y = el.numericalValue();
    if (x < y) return -1;
    else if (x > y) return 1;
    else return 0;
  }

  /**
   * We say that two ExpressionElement instances are equal if their result
   * is the same.
   */
  @Override
  public boolean equals(Object el)
  {
    return el instanceof ExpressionElement &&
      numericalValue() == ((ExpressionElement)el).numericalValue();
  }
}
