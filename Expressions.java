/**
 * @author Alejandro Suarez Hernandez
 *
 * This program calculates all the possible non negative results that can be
 * obtained with n occurrences of the natural c. The available operators are
 * the sum, difference, product and quotient. We allow fully parenthesized
 * expressions.
 *
 * n and c are passed as command line arguments to the program. The program
 * will output all the possible results and, for each one, an expression
 * that produces this result. E.g. for n = 3, c = 3, the program will
 * output:
 * There are 9 possible results
 * 0 = (3*(3-3))
 * 2 = (3-(3/3))
 * 3 = (3+(3-3))
 * 4 = (3+(3/3))
 * 6 = ((3*3)-3)
 * 9 = (3+(3+3))
 * 12 = (3+(3*3))
 * 18 = (3*(3+3))
 * 27 = (3*(3*3))
 */

import java.util.List;
import java.util.ArrayList;

public class Expressions
{
  public static void printUsageAndExit(Throwable ex)
  {
    if (ex != null)
    {
      System.out.println(ex.getMessage());
    }
    System.out.println("Usage: java Expressions num1 num2");
    System.exit(-1);
  }

  public static void main(String[] args)
  {
    /* Read arguments from command line */
    if (args.length != 2) printUsageAndExit(null);
    int n = 0;
    int c = 0;
    try
    {
      n = Integer.valueOf(args[0]);
      c = Integer.valueOf(args[1]);
    }
    catch (NumberFormatException ex)
    {
      printUsageAndExit(ex);
    }

    /* Create and initialize list with levels. Each level levels[i] will
     * contain all the possible results that can be obtained with:
     * i occurrences of c.*/
    List<Level> levels = new ArrayList<>(n+1);
    for (int idx = 0; idx <= n; ++idx)
    {
      levels.add(new Level());
    }
    levels.get(1).add(c);

    for (int idx = 2; idx <= n; ++idx)
    {
      for (int jdx = 1; jdx < idx; ++jdx)
      {
        levels.get(idx).addAll(
            Level.combine(levels.get(jdx),levels.get(idx-jdx)));
      }
    }

    /* Print results */
    System.out.println("There are " + levels.get(n).size() +
        " possible results");
    for (ExpressionElement el : levels.get(n))
    {
      System.out.println(el.numericalValue() + " = " + el);
    }
  }
}
