/**
 * @author Alejandro Suarez Hernandez
 *
 * ExpressionAtom class
 */

/**
 *  This class represents what we call an expression atom: a single number
 */
public class ExpressionAtom extends ExpressionElement
{
  private final int number_;

  public ExpressionAtom()
  {
    number_ = 0;
  }

  public ExpressionAtom(int number)
  {
    number_ = number;
  }

  @Override
  public int numericalValue()
  {
    return number_;
  }

  @Override
  public String toString()
  {
    return String.valueOf(number_);
  }
}
