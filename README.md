# Fully parenthesized expressions

The aim of this program is to obtain all the possible non-negative results
that can be obtained with fully parenthesized expressions with n occurrences
of the number c that use the following operators: sum, product, difference and
quotient. n and c are given by the user as command line arguments.

A dynamic programming approach is used to accomplish this task.

# To compile

Just run `javac *.java`. It has been tested only with Java 7, although it
will probably work with Java 6 and Java 8 as well.

# To run

Execute `java Expressions n c` where n and c are the previously described
parameters. Example: `java Expressions 3 2` will output the following:

    There are 6 possible results
    0 = (2*(2-2))
    1 = (2-(2/2))
    2 = (2+(2-2))
    3 = (2+(2/2))
    6 = (2+(2+2))
    8 = (2*(2+2))