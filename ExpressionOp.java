public class ExpressionOp extends ExpressionElement
{
  private final char op_;
  private final ExpressionElement el1_;
  private final ExpressionElement el2_;
  private final int result_;

  public static final char[] AVAILABLE_OPS = {'+','-','*','/'};

  public ExpressionOp(char op, ExpressionElement el1, ExpressionElement el2)
  {
    op_  = op;
    el1_ = el1;
    el2_ = el2;
    int x = el1_.numericalValue();
    int y = el2_.numericalValue();
    switch (op)
    {
      case '+':
        result_ = x+y;
        break;
      case '-':
        result_ = x-y;
        break;
      case '*':
        result_ = x*y;
        break;
      case '/':
        result_ = (y>0 && (x%y==0))? x/y : -1;
        break;
      default:
        result_ = -1;
    }
  }

  /**
   * @return true if the operation is feasible, false otherwise. I.e. true for
   * sum, products, positive differences and integer divisions; and false
   * for negative differences, non integer divisions or divisions by 0.
   */
  public boolean canDo()
  {
    return result_ >= 0;
  }

  @Override
  public int numericalValue()
  {
    return result_;
  }

  @Override
  public String toString()
  {
    return "("+el1_+op_+el2_+")";
  }
  
}
