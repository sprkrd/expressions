/** @author Alejandro Suarez Hernandez */

import java.util.TreeSet;

public class Level extends TreeSet<ExpressionElement>
{
  /**
   * @return a new level that combines the elements of lv1 and the elements
   * of lv2 in all the possible ways (all with all, using all the available
   * operations when possible).
   */
  public static Level combine(Level lv1, Level lv2)
  {
    Level comblevel = new Level();
    for (ExpressionElement el1 : lv1)
    {
      for (ExpressionElement el2 : lv2)
      {
        for (char op : ExpressionOp.AVAILABLE_OPS)
        {
          ExpressionOp result = new ExpressionOp(op,el1,el2);
          if (result.canDo()) comblevel.add(result);
        }
      }
    }
    return comblevel;
  }

  public void add(int atom)
  {
    add(new ExpressionAtom(atom));
  }
}
